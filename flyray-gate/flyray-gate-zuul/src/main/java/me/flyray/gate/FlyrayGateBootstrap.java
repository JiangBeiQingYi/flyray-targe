package me.flyray.gate;


import me.flyray.gate.ratelimit.EnableAceGateRateLimit;
import me.flyray.gate.ratelimit.config.IUserPrincipal;
import me.flyray.auth.client.EnableAceAuthClient;
import me.flyray.gate.config.UserPrincipal;
import me.flyray.gate.utils.DBLog;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Ace on 2017/6/2.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"me.flyray.auth.client.feign","me.flyray.gate.feign"})
@EnableZuulProxy
@EnableScheduling
@ComponentScan({"me.flyray.gate","me.flyray.cache","me.flyray.auth.client"})
@EnableAceAuthClient
@EnableAceGateRateLimit
public class FlyrayGateBootstrap {
    public static void main(String[] args) {
        DBLog.getInstance().start();
        SpringApplication.run(FlyrayGateBootstrap.class, args);
    }

    @Bean
    @Primary
    IUserPrincipal userPrincipal(){
        return new UserPrincipal();
    }
}
