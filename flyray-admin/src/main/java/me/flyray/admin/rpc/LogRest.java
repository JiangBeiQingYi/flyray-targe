package me.flyray.admin.rpc;

import me.flyray.admin.biz.GateLogBiz;
import me.flyray.admin.entity.GateLog;
import me.flyray.admin.rpc.service.PasswordUtil;
import me.flyray.common.vo.LogInfo;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-07-01 14:39
 */
@RequestMapping("api")
@RestController
public class LogRest {
    @Autowired
    private GateLogBiz gateLogBiz;
    @RequestMapping(value="/log/save",method = RequestMethod.POST)
    public @ResponseBody void saveLog(@RequestBody LogInfo info){
        GateLog log = new GateLog();
        BeanUtils.copyProperties(info,log);
        gateLogBiz.insertSelective(log);
    }


    public static final String SALT="HZMetro_DtDreamHJOJHDJ)*UKI&HBHJUM<BTXUDHGE*&^#*EHHJS";

    public static String getPassword(String password,Integer userId){
        password = DigestUtils.sha512Hex(password+"HZMetro_DtDream&(SJDJ^EHDJOO#KSDISDJF#*(OSJsisj82*AS");
        if (password.length()>=10) {
            return DigestUtils.sha512Hex(password.substring(0, 9)+SALT+password.substring(9)+userId);
        }else {
            return "";
        }
    }

    public static void main(String[] args) {
        System.out.println(PasswordUtil.getPassword("hz123admin", 14));
    }

}
