package me.flyray.admin.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author: bolei
 * @date: 17:03 2019/3/15
 * @Description: 类描述
 */

@Data
public class OrgStructureNode {

    private String value;

    private String name;

    private String parentId;

    private String id;

    private List<OrgStructureNode> children;

}
