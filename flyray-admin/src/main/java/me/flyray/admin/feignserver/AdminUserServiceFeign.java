package me.flyray.admin.feignserver;

import me.flyray.admin.biz.UserBiz;
import me.flyray.admin.entity.User;
import me.flyray.auth.common.user.UserInfo;
import me.flyray.common.constant.UserConstant;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.SnowFlake;
import me.flyray.common.vo.admin.AdminUserRequest;
import me.flyray.common.vo.admin.AdminUserResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 16:35 2019/2/13
 * @Description: 运营后台管理员远程服务实现
 */

@Controller
@RequestMapping("feign/user")
public class AdminUserServiceFeign {

    @Autowired
    private UserBiz userBiz;
    @Value("${admin.password}")
    private String defaultPassword;

    /**
     * 添加后台登陆用户的同时必须指定角色
     * @param param
     * @return
     */
    @RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<AdminUserResponse> addUser(@RequestBody Map<String, Object> param){
        User user = new User();
        if (StringUtils.isEmpty(param.get("password"))){
            param.put("password",defaultPassword);
        }
        try {
            param.put("userId",String.valueOf(SnowFlake.getId()));
            user = userBiz.add(param);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AdminUserResponse adminUserResponse = new AdminUserResponse();
        BeanUtils.copyProperties(user,adminUserResponse);
        return BaseApiResponse.newSuccess(adminUserResponse);
    }

    /**
     * 根据条件查询用户
     */
    @RequestMapping(value = "list",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<List<AdminUserResponse>> getUserList(@RequestBody Map<String, Object> param){
        User entity = EntityUtils.map2Bean(param, User.class);
        List<User> list = userBiz.selectList(entity);
        List<AdminUserResponse> users = new ArrayList<>();
        if(!CollectionUtils.isEmpty(list)) {
            for (User user : list){
                AdminUserResponse userResponse = new AdminUserResponse();
                BeanUtils.copyProperties(user,userResponse);
                users.add(userResponse);
            }
        }
        return BaseApiResponse.newSuccess(users);
    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse deleteByUserId(String userId){
        User user = new User();
        user.setUserId(userId);
        userBiz.delete(user);
        return BaseApiResponse.newSuccess();
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse update(@RequestBody AdminUserRequest adminUserRequest){
        User user = new User();
        BeanUtils.copyProperties(adminUserRequest,user);
        userBiz.updateSelectiveById(user);
        return BaseApiResponse.newSuccess();
    }

    @RequestMapping(value = "/queryByMobile", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> validate(@RequestBody Map<String,String> body){
        Map<String, Object> result = new HashMap<>();
        String mobilePhone = body.get("mobilePhone");
        User user = userBiz.getUserByMobilePhone(mobilePhone);
        if (null != user) {
            UserInfo info = new UserInfo();
            BeanUtils.copyProperties(user, info);
            info.setId(user.getId().toString());
            result.put("info", info);
        }else{
            result.put("info", null);
        }
        result.put("code", ResponseCode.OK.getCode());
        result.put("message", ResponseCode.OK.getMessage());
        return result;
    }
}
