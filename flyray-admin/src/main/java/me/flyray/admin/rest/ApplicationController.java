package me.flyray.admin.rest;

import lombok.extern.slf4j.Slf4j;
import me.flyray.admin.biz.*;
import me.flyray.admin.constant.AdminCommonConstant;
import me.flyray.admin.entity.BaseApplication;
import me.flyray.admin.entity.Menu;
import me.flyray.admin.entity.ResourceAuthority;
import me.flyray.admin.entity.UserRole;
import me.flyray.admin.vo.MenuTree;
import me.flyray.auth.common.config.UserAuthConfig;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.common.util.TreeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 19:17 2019/3/5
 * @Description: 接入的应用
 */

@Slf4j
@RestController
@RequestMapping("app")
public class ApplicationController extends BaseController<BaseApplicationBiz, BaseApplication> {


    @Autowired
    private MenuBiz menuBiz;
    @Autowired
    private UserRoleBiz userRoleBiz;
    @Autowired
    private UserAuthConfig userAuthConfig;
    @Autowired
    private ResourceAuthorityBiz resourceAuthorityBiz;

    /**
     * 获取应用下的菜单树
     * @param param
     * @return
     */
    @RequestMapping(value = "/menuTree", method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse getTree(@RequestParam Map<String, Object> param) {
        Example example = new Example(Menu.class);
        String title = (String) param.get("title");
        if (StringUtils.isNotBlank(title)) {
            example.createCriteria().andLike("title", "%" + title + "%");
        }
        if (!org.springframework.util.StringUtils.isEmpty(param.get("appId"))){
            example.createCriteria().andLike("appId", (String) param.get("appId"));
        }else {
            example.createCriteria().andLike("appId", "a1");
        }
        List<MenuTree> list = getMenuTree2(menuBiz.selectByExample(example), AdminCommonConstant.ROOT,request.getHeader(userAuthConfig.getTokenHeader()));
        return BaseApiResponse.newSuccess(list);
    }

    private List<MenuTree> getMenuTree2(List<Menu> menus,int root,String token) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        String roleId = "0";
        String userId = BaseContextHandler.getXId();
        UserRole userRole = new UserRole();
        userRole.setUserId(userId);
        List<UserRole>  userRoleList = userRoleBiz.selectList(userRole);
        UserRole userRoleOther = userRoleList.get(0);
        roleId = userRoleOther.getRoleId();
        ResourceAuthority resourceAuthority = new ResourceAuthority();
        resourceAuthority.setAuthorityId(String.valueOf(roleId));
        resourceAuthority.setResourceType("menu");
        List<ResourceAuthority> resourceAuthorityList = resourceAuthorityBiz.selectList(resourceAuthority);
        for (Menu menu : menus) {
            //判断一下有没有这个菜单的权限
            if(!"1".equals(roleId)){
                for (ResourceAuthority ra : resourceAuthorityList) {
                    if(menu.getId() == Integer.valueOf(ra.getResourceId())){
                        node = new MenuTree();
                        BeanUtils.copyProperties(menu, node);
                        node.setLabel(menu.getTitle());
                        trees.add(node);
                        break;
                    }
                }
            }else {
                node = new MenuTree();
                BeanUtils.copyProperties(menu, node);
                node.setId(String.valueOf(menu.getId()));
                node.setParentId(String.valueOf(menu.getParentId()));
                node.setLabel(menu.getTitle());
                trees.add(node);
            }
        }
        return TreeUtil.bulid(trees,root) ;
    }


}
