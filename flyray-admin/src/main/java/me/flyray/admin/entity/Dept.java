package me.flyray.admin.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "base_dept")
public class Dept {

    @Column(name = "id")
    private Integer id;

    /**
     * 部门ID
     */
    @Id
    @Column(name = "dept_id")
    private String deptId;

    /**
     * 所属平台自增ID
     */
    @Column(name = "platform_id")
    private String platformId;

    /**
     * 上级部门ID，一级部门为0
     */
    @Column(name = "parent_id")
    private String parentId;

    /**
     * 上级部门名称
     */
    @Transient
    private String parentName;

    /**
     * 部门名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 排序
     */
    @Column(name = "sort_num")
    private Integer sortNum;

    /**
     * 是否删除  -1：已删除  0：正常
     */
    @Column(name = "del_flag")
    private Byte delFlag;

}