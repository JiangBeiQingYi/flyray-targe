package me.flyray.admin.biz;

import me.flyray.admin.constant.AdminCommonConstant;
import me.flyray.admin.entity.Menu;
import me.flyray.admin.mapper.MenuMapper;
import me.flyray.admin.vo.AuthorityMenuTree;
import me.flyray.common.biz.BaseBiz;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-12 8:48
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MenuBiz extends BaseBiz<MenuMapper, Menu> {
    @Override
    public List<Menu> selectListAll() {
        return super.selectListAll();
    }

    @Override
    public int insertSelective(Menu entity) {
        if (AdminCommonConstant.ROOT == entity.getParentId()) {
            entity.setPath("/" + entity.getCode());
        } else {
            Menu parent = this.selectById(entity.getParentId());
            entity.setPath(parent.getPath() + "/" + entity.getCode());
        }
        return super.insertSelective(entity);
    }

    @Override
    public void updateById(Menu entity) {
        if (AdminCommonConstant.ROOT == entity.getParentId()) {
            entity.setPath("/" + entity.getCode());
        } else {
            Menu parent = this.selectById(entity.getParentId());
            entity.setPath(parent.getPath() + "/" + entity.getCode());
        }
        super.updateById(entity);
    }

    @Override
    public void updateSelectiveById(Menu entity) {
        super.updateSelectiveById(entity);
    }

    /**
     * 获取用户可以访问的菜单
     *
     * @param id
     * @return
     */
    public List<Menu> getUserAuthorityMenuByUserId(String id) {
        return mapper.selectAuthorityMenuByUserId(id);
    }

    /**
     * 获取用户在appId下可以访问的菜单
     *
     * @param userId  appId
     * @return
     */
    public List<Menu> getAuthorityAppMenuByUserIdAppId(String userId, String appId) {
        return mapper.getAuthorityAppMenuByUserIdAppId(userId,appId);
    }

    /**
     * 获取role拥有的应用菜单权限
     * @param id
     * @return
     */
    public List<AuthorityMenuTree> getRoleAppAuthorityMenu(String roleId, String appId) {
        List<Menu> menus = mapper.selectMenuByAuthorityIdAppId(roleId, appId, AdminCommonConstant.AUTHORITY_TYPE_GROUP);
        List<AuthorityMenuTree> trees = new ArrayList<AuthorityMenuTree>();
        AuthorityMenuTree node = null;
        for (Menu menu : menus) {
            node = new AuthorityMenuTree();
            BeanUtils.copyProperties(menu, node);
            node.setText(menu.getTitle());
            node.setId(String.valueOf(menu.getId()));
            node.setParentId(String.valueOf(menu.getParentId()));
            trees.add(node);
        }
        return trees;
    }


    /**
     * 根据用户获取可以访问的系统
     *
     * @param id
     * @return
     */
    public List<Menu> getUserAuthoritySystemByUserId(int id) {
        return mapper.selectAuthoritySystemByUserId(id);
    }
}
