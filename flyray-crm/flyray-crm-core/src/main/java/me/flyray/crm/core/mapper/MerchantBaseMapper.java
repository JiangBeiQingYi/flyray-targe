package me.flyray.crm.core.mapper;

import java.util.List;
import java.util.Map;

import me.flyray.crm.core.entity.MerchantBase;

import tk.mybatis.mapper.common.Mapper;

/**
 * 商户客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantBaseMapper extends Mapper<MerchantBase> {
	
	public List<MerchantBase> queryByProvince(Map<String, Object> map);
	
}
