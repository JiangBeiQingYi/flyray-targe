package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PlatformBaseExtend;
import tk.mybatis.mapper.common.Mapper;

/**
 * 平台扩展信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformBaseExtendMapper extends Mapper<PlatformBaseExtend> {
	
}
