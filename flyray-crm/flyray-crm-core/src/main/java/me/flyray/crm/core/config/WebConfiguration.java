package me.flyray.crm.core.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import me.flyray.common.exception.GlobalExceptionHandler;
import me.flyray.common.interceptor.ValidateParamInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import me.flyray.auth.client.interceptor.ServiceAuthRestInterceptor;
import me.flyray.auth.client.interceptor.UserAuthRestInterceptor;

/**
 *
 * @author ace
 * @date 2017/9/8
 */
@Configuration("crmCoreWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {
    @Bean
    GlobalExceptionHandler getGlobalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        ArrayList<String> commonExcludePathPatterns = getExcludeCommonPathPatterns();
        registry.addInterceptor(getServiceAuthRestInterceptor()).
                addPathPatterns("/**").excludePathPatterns(commonExcludePathPatterns.toArray(new String[]{}));
        commonExcludePathPatterns.add("/feign/**");
        registry.addInterceptor(getUserAuthRestInterceptor()).
                addPathPatterns("/**").excludePathPatterns(commonExcludePathPatterns.toArray(new String[]{}));
    }

    @Bean
    ServiceAuthRestInterceptor getServiceAuthRestInterceptor() {
        return new ServiceAuthRestInterceptor();
    }

    @Bean
    UserAuthRestInterceptor getUserAuthRestInterceptor() {
        return new UserAuthRestInterceptor();
    }
    
    @Bean
    ValidateParamInterceptor getValidateParamInterceptor() {
        return new ValidateParamInterceptor();
    }
    /**
     * 需要用户和服务认证判断的路径
     * @return
     */
    private ArrayList<String> getIncludePathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/**"
        };
        Collections.addAll(list, urls);
        return list;
    }
    
    private ArrayList<String> getExcludeCommonPathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/v2/api-docs",
                "/api/user/validate",
                "/swagger-resources/**",
                "/cache/**",
                "/api/log/save",
                "/activity/*",
                "/coupon/*",
                "/incident/*",
                "/integral/*",
                "/common/*",
                "/marScene/*",
                "/job/*",
                "/scene/*",
                "/jobLog/*",
                "/customer/queryCustomerinfoByMobile"
        };
        Collections.addAll(list, urls);
        return list;
    }

	/*@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		WebMvcConfigurer.super.addResourceHandlers(registry);
		registry.addResourceHandler("/uploadFile/**").addResourceLocations("file:D:/pj_bounty/bounty-hunter/flyray-crm-core/pics/");
	}*/

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        //创建fastJson消息转换器
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        List<MediaType> supportedMediaTypes = new ArrayList<>();
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
        supportedMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        supportedMediaTypes.add(MediaType.APPLICATION_ATOM_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
        supportedMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
        supportedMediaTypes.add(MediaType.APPLICATION_PDF);
        supportedMediaTypes.add(MediaType.APPLICATION_RSS_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_XHTML_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_XML);
        supportedMediaTypes.add(MediaType.IMAGE_GIF);
        supportedMediaTypes.add(MediaType.IMAGE_JPEG);
        supportedMediaTypes.add(MediaType.IMAGE_PNG);
        supportedMediaTypes.add(MediaType.TEXT_EVENT_STREAM);
        supportedMediaTypes.add(MediaType.TEXT_HTML);
        supportedMediaTypes.add(MediaType.TEXT_MARKDOWN);
        supportedMediaTypes.add(MediaType.TEXT_PLAIN);
        supportedMediaTypes.add(MediaType.TEXT_XML);
        fastConverter.setSupportedMediaTypes(supportedMediaTypes);
        converters.add(fastConverter);
    }

}
