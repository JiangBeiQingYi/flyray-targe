package me.flyray.crm.core.feignserver;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.exception.BusinessException;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.util.HideDataUtil;
import me.flyray.crm.core.biz.merchant.MerchantBindCardBiz;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.MerchantBindCard;
import me.flyray.crm.core.entity.MerchantBase;
import me.flyray.crm.core.mapper.CustomerBaseMapper;
import me.flyray.crm.facade.request.MerchantBaseRequest;
import me.flyray.crm.facade.request.MerchantBindCardRequest;
import me.flyray.crm.facade.response.CustomerBindCardResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.merchant.MerchantBaseBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 商户客户
 * */
@Api(tags="商户客户")
@Controller
@RequestMapping("feign/merchant")
public class MerchantBaseServiceClientFeign {
	
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	@Autowired
	private MerchantBindCardBiz merchantBindCardBiz;
	@Autowired
	private CustomerBaseMapper customerBaseMapper;

	/**
	 * 商户客户查询
	 * @author centerroot
	 * @time 创建时间:2018年9月26日下午4:31:39
	 * @param merchantBaseRequest
	 * @return
	 */
	@ApiOperation("商户客户查询")
	@RequestMapping(value = "/queryInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryMerchantInfo(@RequestBody @Valid MerchantBaseRequest merchantBaseRequest){
		Map<String, Object> respMap = new HashMap<>();
		MerchantBase merchantBaseReq = new MerchantBase();
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(merchantBaseRequest.getPlatformId());
		customerBase.setCustomerId(merchantBaseRequest.getCustomerId());
		customerBase = customerBaseMapper.selectOne(customerBase);
		merchantBaseReq.setMerchantId(customerBase.getMerchantId());
		BeanUtils.copyProperties(merchantBaseRequest, merchantBaseReq);
		MerchantBase merchantBase = merchantBaseBiz.selectOne(merchantBaseReq);
		if (null == merchantBase) {
			respMap.put("code", ResponseCode.MER_NOTEXIST.getCode());
			respMap.put("message", ResponseCode.MER_NOTEXIST.getMessage());
		}else{
			MerchantBaseRequest merchantBaseEntity = new MerchantBaseRequest();
			BeanUtils.copyProperties(merchantBase, merchantBaseEntity);
			respMap.put("merchantBase", merchantBaseEntity);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("message", ResponseCode.OK.getMessage());
		}
		return respMap;
    }

	/**
	 * 商户客户绑卡
	 * @param request
	 * @return
	 */
	@ApiOperation("商户客户绑卡")
	@RequestMapping(value = "/bindCard",method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse<MerchantBindCard> bindCard(@RequestBody @Valid MerchantBindCardRequest request){
		MerchantBindCard merchantBindCard = new MerchantBindCard();
		//查询客户信息中的商户号和个人编号
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(request.getPlatformId());
		customerBase.setCustomerId(request.getCustomerId());
		customerBase = customerBaseMapper.selectOne(customerBase);
		if(customerBase != null){
			MerchantBase merchantBase = new MerchantBase();
			merchantBase.setPlatformId(request.getPlatformId());
			merchantBase.setCustomerId(request.getCustomerId());
			merchantBase.setMerchantId(customerBase.getMerchantId());
			merchantBase.setMerchantType(request.getMerchantType());
			MerchantBase merchantBase1 = merchantBaseBiz.selectOne(merchantBase);
			if(merchantBase1 != null){
				merchantBase1.setBusinessNo(request.getBusinessLisence());
				merchantBase1.setBusinessLicence(request.getLisenceImageUrl());
				merchantBaseBiz.updateById(merchantBase1);
			}
		}
		BeanUtils.copyProperties(request, merchantBindCard);
		merchantBindCard.setMerchantId(customerBase.getMerchantId());
		merchantBindCardBiz.insert(merchantBindCard);
		return BaseApiResponse.newSuccess(merchantBindCard);
	}

	/**
	 * 商户客户绑卡信息更新
	 * @param request
	 * @return
	 */
	@ApiOperation("商户客户绑卡信息更新")
	@RequestMapping(value = "/bindCard/update",method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse<MerchantBindCard> bindCardUpdate(@RequestBody @Valid MerchantBindCardRequest request){
		MerchantBindCard merchantBindCard = new MerchantBindCard();
		BeanUtils.copyProperties(request, merchantBindCard);
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(request.getPlatformId());
		customerBase.setCustomerId(request.getCustomerId());
		customerBase = customerBaseMapper.selectOne(customerBase);
		if(customerBase != null){
			MerchantBase merchantBase = new MerchantBase();
			merchantBase.setPlatformId(request.getPlatformId());
			merchantBase.setCustomerId(request.getCustomerId());
			merchantBase.setMerchantId(customerBase.getMerchantId());
			merchantBase.setMerchantType(request.getMerchantType());
			MerchantBase merchantBase1 = merchantBaseBiz.selectOne(merchantBase);
			if(merchantBase1 != null){
				merchantBase1.setBusinessNo(request.getBusinessLisence());
				merchantBase1.setBusinessLicence(request.getLisenceImageUrl());
				merchantBaseBiz.updateById(merchantBase1);
			}
		}
		merchantBindCard.setMerchantId(customerBase.getMerchantId());
		merchantBindCardBiz.updateSelectiveById(merchantBindCard);
		return BaseApiResponse.newSuccess(merchantBindCard);
	}


	/**
	 * 商户客户绑卡信息查询
	 * @param request
	 * @return
	 */
	@ApiOperation("商户客户绑卡信息查询")
	@RequestMapping(value = "/bindCard/Info",method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse<CustomerBindCardResponse> bindCardInfo(@RequestBody @Valid MerchantBindCardRequest request){
		MerchantBindCard merchantBindCard = new MerchantBindCard();
		CustomerBase customerBase = new CustomerBase();
		customerBase.setPlatformId(request.getPlatformId());
		customerBase.setCustomerId(request.getCustomerId());
		customerBase = customerBaseMapper.selectOne(customerBase);
		MerchantBase merchantBase1 = null;
		if(customerBase != null){
			MerchantBase merchantBase = new MerchantBase();
			merchantBase.setPlatformId(request.getPlatformId());
			merchantBase.setCustomerId(request.getCustomerId());
			merchantBase.setMerchantId(customerBase.getMerchantId());
			merchantBase.setMerchantType(request.getMerchantType());
			merchantBase1 = merchantBaseBiz.selectOne(merchantBase);
		}
		BeanUtils.copyProperties(request, merchantBindCard);
		merchantBindCard.setMerchantId(customerBase.getMerchantId());
		HideDataUtil.hideCardNo(request.getIdCardNo());
		merchantBindCard.setEncryptBankCardNo(HideDataUtil.hideCardNo(request.getBeneficiaryBankCardNo()));
		merchantBindCard = merchantBindCardBiz.selectOne(merchantBindCard);
		CustomerBindCardResponse customerBindCardResponse = new CustomerBindCardResponse();
		if (null == merchantBindCard) {
			return BaseApiResponse.newSuccess(null);
//			return BaseApiResponse.newFailure(ResponseCode.MERCHANT_BIND_CARD_ISEXIST);
//			throw new BusinessException(ResponseCode.MERCHANT_BIND_CARD_ISEXIST.getCode());
		}
		BeanUtils.copyProperties(merchantBindCard,customerBindCardResponse);
		if(merchantBase1 != null){
			customerBindCardResponse.setBusinessLisence(merchantBase1.getBusinessNo());
			customerBindCardResponse.setLisenceImageUrl(merchantBase1.getBusinessLicence());
		}
		return BaseApiResponse.newSuccess(customerBindCardResponse);
	}
	
	
}
