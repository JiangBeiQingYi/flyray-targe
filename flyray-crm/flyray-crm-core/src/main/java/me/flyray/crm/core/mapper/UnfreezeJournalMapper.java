package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.UnfreezeJournal;
import tk.mybatis.mapper.common.Mapper;

/**
 * 解冻流水表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface UnfreezeJournalMapper extends Mapper<UnfreezeJournal> {
	
}
