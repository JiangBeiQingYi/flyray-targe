package me.flyray.crm.core.biz.merchant;

import me.flyray.common.biz.BaseBiz;
import me.flyray.crm.core.entity.MerchantBindCard;
import me.flyray.crm.core.mapper.MerchantBindCardMapper;
import org.springframework.stereotype.Service;


/**
 * 
 *
 * @author admin
 * @email ${email}
 * @date 2019-01-07 17:41:33
 */
@Service
public class MerchantBindCardBiz extends BaseBiz<MerchantBindCardMapper, MerchantBindCard> {
}