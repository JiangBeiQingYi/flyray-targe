package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import me.flyray.common.entity.BaseEntity;
import lombok.Data;


/**
 * 客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "customer_base")
@Data
public class CustomerBase extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
    //序号
    @Id
    private Integer id;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空
    @Column(name = "customer_type")
    private String customerType;

	/**
	 * 登陆用户名
	 */
	@Column(name = "username")
	private String username;

	//登陆密码
	@Column(name = "password")
	private String password;
	
	    //支付密码
    @Column(name = "pay_password")
    private String payPassword;
	
	    //登录密码错误次数
    @Column(name = "password_error_count")
    private Integer passwordErrorCount;
	
	    //支付密码错误次数
    @Column(name = "pay_password_error_count")
    private Integer payPasswordErrorCount;
	
	    //登录密码状态  00：正常   01：未设置   02：锁定
    @Column(name = "password_status")
    private String passwordStatus;
	
	    //支付密码状态    00：正常   01：未设置   02：锁定
    @Column(name = "pay_password_status")
    private String payPasswordStatus;
	
	    //个人客户编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //商户客户编号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //账户状态 00：正常，01：客户冻结
    @Column(name = "status")
    private String status;
	
	    //认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @Column(name = "authentication_status")
    private String authenticationStatus;
	
	    //注册时间
    @Column(name = "register_time")
    private Date registerTime;
	
	    //注册IP
    @Column(name = "register_ip")
    private String registerIp;
	
	    //上次登录时间
    @Column(name = "login_time")
    private Date loginTime;
	
	    //上次登录IP
    @Column(name = "login_ip")
    private String loginIp;
	
	    //最后登录角色（默认为空）  01：个人客户     02 ：商户客户
    @Column(name = "last_login_role")
    private String lastLoginRole;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	
	    //登录密码盐值
	@Column(name = "password_salt")
	private String passwordSalt;
	
		//支付密码盐值
	@Column(name = "pay_password_salt")
	private String payPasswordSalt;

    /**
     * 供业务拓展使用自定义
     */
    @Column(name = "role_type")
    private Integer roleType;

}
