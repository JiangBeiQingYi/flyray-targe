package me.flyray.crm.core.modules.platform;

import me.flyray.crm.core.biz.platform.PlatformCoinTransactionFromBiz;
import me.flyray.crm.core.entity.PlatformCoinTransactionFrom;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.flyray.common.rest.BaseController;

@Controller
@RequestMapping("platformCoinTransactionFrom")
public class PlatformCoinTransactionFromController extends BaseController<PlatformCoinTransactionFromBiz, PlatformCoinTransactionFrom> {

}