package me.flyray.crm.core.mapper;

import java.util.List;
import java.util.Map;

import me.flyray.crm.core.entity.PersonalGjjInfos;

import tk.mybatis.mapper.common.Mapper;

/**
 * 用户公积金基本信息管理
 */
public interface PersonalGjjMapper extends Mapper<PersonalGjjInfos> {

	public List<PersonalGjjInfos> queryCustBaseList(Map<String, Object> request);
}