package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Table(name = "platform_coin_accout")
public class PlatformCoinAccout implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //平台编号
    @Id
    private Long platformId;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //平台类型：1 顶级平台 2 普通平台
    @Column(name = "platform_type")
    private String platformType;
	
	    //平台合约地址
    @Column(name = "address")
    private String address;
	
	    //平台账号类型：1平台总量 2手续费
    @Column(name = "account_type")
    private String accountType;
	
	    //账户余额
    @Column(name = "account_balance")
    private BigDecimal accountBalance;
	
	    //冻结金额
    @Column(name = "freeze_balance")
    private BigDecimal freezeBalance;
	
	    //校验码（余额加密值）
    @Column(name = "check_sum")
    private String checkSum;
	
	    //账户状态 00：正常，01：冻结
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：平台类型：1 顶级平台 2 普通平台
	 */
	public void setPlatformType(String platformType) {
		this.platformType = platformType;
	}
	/**
	 * 获取：平台类型：1 顶级平台 2 普通平台
	 */
	public String getPlatformType() {
		return platformType;
	}
	/**
	 * 设置：平台合约地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：平台合约地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：平台账号类型：1平台总量 2手续费
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * 获取：平台账号类型：1平台总量 2手续费
	 */
	public String getAccountType() {
		return accountType;
	}
	/**
	 * 设置：账户余额
	 */
	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}
	/**
	 * 获取：账户余额
	 */
	public BigDecimal getAccountBalance() {
		return accountBalance;
	}
	/**
	 * 设置：冻结金额
	 */
	public void setFreezeBalance(BigDecimal freezeBalance) {
		this.freezeBalance = freezeBalance;
	}
	/**
	 * 获取：冻结金额
	 */
	public BigDecimal getFreezeBalance() {
		return freezeBalance;
	}
	/**
	 * 设置：校验码（余额加密值）
	 */
	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}
	/**
	 * 获取：校验码（余额加密值）
	 */
	public String getCheckSum() {
		return checkSum;
	}
	/**
	 * 设置：账户状态 00：正常，01：冻结
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：账户状态 00：正常，01：冻结
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
