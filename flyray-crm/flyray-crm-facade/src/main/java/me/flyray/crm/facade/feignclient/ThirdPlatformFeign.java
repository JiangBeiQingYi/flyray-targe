package me.flyray.crm.facade.feignclient;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.crm.facade.request.*;
import me.flyray.crm.facade.response.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * Description: 
 *
 * @param:
 * @return: 
 * @date: 2019-01-17 9:20
 */

@FeignClient(value = "flyray-crm-core")
public interface ThirdPlatformFeign {

    /**
     *  运营查询账户列表（分页）
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/platform/list_account",method = RequestMethod.POST)
    TableResultResponse<CustomerAccountQueryResponse> listAccount(@RequestBody MerchantAccountOpRequest param);

    /**
     *  查询账户总额
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/platform/account_balance",method = RequestMethod.POST)
    BaseApiResponse<MerchantAccountOpBalanceResp> accountBalance(@RequestBody MerchantBalanceOpRequest param);

    /**
     *  运营查询流水（分页）
     * @param param
     * @return
     */
    @RequestMapping(value = "feign/platform/list_journal",method = RequestMethod.POST)
    TableResultResponse<MerchantAccountOpJournalResponse> listJournal(@RequestBody MerchantAccountOpJournalRequest param);

}
