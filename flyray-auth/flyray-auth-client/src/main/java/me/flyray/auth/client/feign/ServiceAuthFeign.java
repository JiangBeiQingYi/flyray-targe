package me.flyray.auth.client.feign;

import me.flyray.auth.common.user.UserInfo;
import me.flyray.auth.common.vo.JwtAuthenticationResponse;
import me.flyray.common.msg.BaseApiResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by ace on 2017/9/15.
 */
@FeignClient(value = "${auth.serviceId}",configuration = {})
public interface ServiceAuthFeign {

    @RequestMapping(value = "feign/auth/getAdminUserToken",method = RequestMethod.POST)
    public BaseApiResponse<JwtAuthenticationResponse> getAdminUserToken(@RequestBody UserInfo param);

    @RequestMapping(value = "feign/auth/getBizUserToken",method = RequestMethod.POST)
    public BaseApiResponse<JwtAuthenticationResponse> getBizUserToken(@RequestBody UserInfo param);

    @RequestMapping(value = "feign/auth/refresh",method = RequestMethod.POST)
    public BaseApiResponse<String> refresh(String oldToken);

    @RequestMapping(value = "feign/auth/verify",method = RequestMethod.POST)
    public BaseApiResponse verify(String token);

    @RequestMapping(value = "/client/myClient")
    public BaseApiResponse<List<String>> getAllowedClient(@RequestParam("serviceId") String serviceId, @RequestParam("secret") String secret);

    @RequestMapping(value = "/client/token",method = RequestMethod.POST)
    public BaseApiResponse getAccessToken(@RequestParam("clientCode") String clientCode, @RequestParam("secret") String secret);

    @RequestMapping(value = "/client/servicePubKey",method = RequestMethod.POST)
    public BaseApiResponse<byte[]> getServicePublicKey(@RequestParam("clientId") String clientId, @RequestParam("secret") String secret);

    @RequestMapping(value = "/client/userPubKey",method = RequestMethod.POST)
    public BaseApiResponse<byte[]> getUserPublicKey(@RequestParam("clientId") String clientId, @RequestParam("secret") String secret);

}
