package me.flyray.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 查询用户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "修改客户等级")
public class PersonalLevelRequest implements Serializable {

	@NotNull(message = "参数[personalLevel]不能为空")
	@ApiModelProperty(value = "客户等级")
	private String personalLevel;

	@Size(max = 50 )
	@NotNull(message = "参数[personalIds]不能为空")
	@ApiModelProperty(value = "个人客户编号列表")
	private String[] personalIds;
}
