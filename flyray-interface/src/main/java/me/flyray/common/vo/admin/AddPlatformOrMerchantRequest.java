package me.flyray.common.vo.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 14:12 2019/2/14
 * @Description: 类描述
 */

@Data
public class AddPlatformOrMerchantRequest implements Serializable {

    @ApiModelProperty(value = "平台编号")
    private String platformId;

    @ApiModelProperty(value = "平台名称")
    private String platformName;

    //平台简介
    @ApiModelProperty(value = "平台简介")
    private String platformIntroduction;

    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @ApiModelProperty(value = "认证状态")
    private String authenticationStatus;

    //最后操作人编号
    @ApiModelProperty(value = "最后操作人编号")
    private Integer operatorId;

    //最后操作人名称
    @ApiModelProperty(value = "最后操作人名称")
    private String operatorName;

    //创建时间
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    //更新时间
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    //平台级别  01:一级平台（顶级） 02：二级平台（默认）
    @ApiModelProperty(value = "平台级别 ")
    private String platformLevel;

    //平台登录名称
    @ApiModelProperty(value = "平台登录名称 ")
    private String platformLoginName;

    //平台图片
    @ApiModelProperty(value = "平台logo")
    private String platformLogoStr;

    //平台手机号
    @ApiModelProperty(value = "平台手机号")
    private String mobilePhone;

    //商户名称
    @ApiModelProperty(value = "商户名称")
    private String merchantName;

    //商户ID
    @ApiModelProperty(value = "商户ID")
    private String merchantId;

    //用户类型
    @ApiModelProperty(value = "用户类型")
    private Integer userType;



}
