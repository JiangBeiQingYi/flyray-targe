package me.flyray.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * 查询用户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "邀请客户查询参数响应")
@JsonInclude(JsonInclude.Include.ALWAYS)
public class OwnerInfoResponse implements Serializable {

	// 归属人id（存后台管理系统登录人员id）指所属客户经理id
	private String ownerId;

	// 归属人名称（存后台管理系统登录人员名称）所属哪个客户经理
	private String ownerName;

	private String realName;

	private String phone;

	private String email;

	private String sex;

}
